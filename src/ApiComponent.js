import { html, css, LitElement } from "lit";

export class ApiComponent extends LitElement {
  static get styles() {
    return css`
      :host {
        display: block;
        padding: 25px;
        color: var(--api-component-text-color, #000);
      }
      table {
        border: 1px solid;
        padding: 10px;
      }
      td {
        border: 1px solid gray;
        padding: 10px;
      }

      thead td {
        background-color: skyblue;
        font-weight: bold;
      }
    `;
  }

  static get properties() {
    return {
      datos: { type: Object },
    };
  }

  constructor() {
    super();
    var n = {};
    n.name = this.datos;
  }

  async getApi() {
    const response = await fetch("https://api.datos.gob.mx/v1/calidadAire");
    const body = await response.json();
    console.log(body);
    this.data = body.results;
    console.log(this.data);
    this.datos = this.data;
  }

  connectedCallback() {
    super.connectedCallback();
    this.getApi();
  }
  render(datos) {
    return html`
      <table>
        <thead>
          <td>id#</td>
          <td>name</td>
          <td>source_id</td>
          <td>scale</td>
          <td>value</td>
        </thead>
        <tbody id="datos">
          <tr>
            <td>${this.datos[0]._id}</td>
            <td>${this.datos[0].stations[0].name}</td>
            <td>${this.datos[0].stations[0].source_id}</td>
            <td>${this.datos[0].stations[0].indexes[0].value}</td>
            <td>${this.datos[0].stations[0].indexes[0].scale}</td>
          </tr>
          <tr>
            <td>${this.datos[1]._id}</td>
            <td>${this.datos[1].stations[0].name}</td>
            <td>${this.datos[1].stations[0].source_id}</td>
            <td>${this.datos[1].stations[0].indexes[0].value}</td>
            <td>${this.datos[1].stations[0].indexes[0].scale}</td>
          </tr>
          <tr>
            <td>${this.datos[2]._id}</td>
            <td>${this.datos[2].stations[0].name}</td>
            <td>${this.datos[2].stations[0].source_id}</td>
            <td>${this.datos[2].stations[0].indexes[0].value}</td>
            <td>${this.datos[2].stations[0].indexes[0].scale}</td>
          </tr>
        </tbody>
      </table>
    `;
  }
}
